const passport = require(`passport`)
const GoogleStrategy = require(`passport-google-oauth20`).Strategy
const User = require('../models/user')

passport.serializeUser((user, done)=>{
  console.log(`Selrialize check ok with user id:${user.id}`)
  done(null, user.id)
})

passport.deserializeUser((id, done)=>{
  console.log(`Deserialize check ok with user id:${id}`)
  User.findById(id).then((user)=>{done(null, user.id)})
})

passport.use(new GoogleStrategy({
  callbackURL: `/login/google/redirect`,
  clientID: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET
}, (accesToken, refreshToken, profile, done) => {
  User.findOne({ googleID: profile.id }).then((currentUser) => {
      //console.log('hi this is callback passport', profile)
      currentUser ? (console.log(`Current user is ${currentUser}`), done(null,currentUser)) : 
      (new User({
        name: profile.displayName,
        googleID: profile.id,
        thumbnail: profile._json.picture 
      })
      .save().then((newUser) => { done(null, newUser) }),
      console.log(`New user created in data base` )
      )})}
))