const { Router } = require('express')
const router = Router()
const passport = require('passport')



router.get('/', (req, res)=>{
  req.user ? res.json(req.user) : res.render('login')
})

router.get('/google', passport.authenticate(`google`, {
scope:['profile']
}))

// router.get(`/google/redirect`, passport.authenticate('google'), (req,res)=>{
//   req.user ? res.redirect(`/api/nextwindow/${req.user._id}`): res.send(`User está mal necesitas meterlo bien`)
// })

// router.get(`/google/redirect`, passport.authenticate('google'), (req,res)=>{
//   req.user ? (res.redirect(`http://localhost:4200`)): res.send(`User está mal necesitas meterlo bien`)
// })

router.get('/google/redirect', passport.authenticate('google'), (req,res)=>{
  req.user ? (res.redirect('https://frontlogger.vercel.app/')): res.send('User está mal necesitas meterlo bien')
})

module.exports = router