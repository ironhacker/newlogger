const { Router } = require('express')
const router = Router()

const authCheck = require('../middlewares/authCheck')
const userControl = require(`../controllers/user.controllers`)

router.get(`/`, userControl.getUsers)

router.post(`/`, userControl.postUser)

router.get('/user/:id', userControl.getUser)

router.put('/user/:id', userControl.putUser)

router.delete('/user/:id', userControl.deleteUser)

router.get('/nextwindow/:id', userControl.getUser)


module.exports = router
