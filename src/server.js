const express = require('express')
const path = require('path')
const routes = require(`./routes/users.routes`)
const loginRoutes = require(`./routes/login.routes`)
const passportSetup = require(`./config/passport-setup`)
const authRoutes = require('./middlewares/authCheck')
//const morgan = require('morgan')
const cors = require ('cors')
const passport = require(`passport`)
const coockieSession = require(`cookie-session`)


//Initialization
const app =  express()

//Initialize passport
app.use(passport.initialize())
app.use(passport.session())

//Settings
var corsOptions = {
  origin:  "http://localhost:4200",
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.set('view engine', 'ejs')
app.set(`port`, process.env.PORT || 5000)
app.set('views', path.join(__dirname, `views`))


//Middlewarea
//app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use(cors())//cors({origin:"url_que_quiero"}) y encontes permite comunicacion con esa URL en concreto

app.use(coockieSession({
  maxAge:24*60*60*1000,
  keys:[process.env.COOKIE_KEY]
}))



//Global variables

//Routes
app.use('/api', routes)
app.use('/login', loginRoutes)

//Static files
app.use(express.static(path.join(__dirname, 'public')))

module.exports = app