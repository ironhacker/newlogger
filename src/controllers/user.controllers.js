const User = require('../models/user')
const UserControl = {}

UserControl.getUsers = async(req, res)=> {
  const users = await User.find()
  res.json(users)
}

UserControl.postUser = async(req, res)=> {
  const newUser =  new User({
    name: req.body.name,
    emails: req.body.emails,
    password: req.body.password,
    cargo: req.body.cargo,
  })
  await newUser.save();
  res.json({ status: "User created" });
  }

UserControl.getUser = async(req, res)=>{
  console.log('THIS IS CONTROLLER ID', req.params.id)
  const userfound = await User.findById(req.params.id)
  res.render('user')
}

UserControl.putUser = async(req, res)=> {
  await User.findByIdAndUpdate(req.params.id, req.body)
  await res.json({status: 'updated db'})
}

UserControl.deleteUser = async(req, res)=>{
  await User.deleteOne({"_id": req.params.id})
  res.json({status: "Deleted element"})
}

module.exports = UserControl